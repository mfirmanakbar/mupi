package com.uin.firman.fedora.mupi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.data.DataMovieDetail;

import java.util.List;

/**
 * Created by koko on 4/13/16.
 *
 * Buat class Adapter.java didalam package adapter dan tambahkan coding seperti dibawah ini.
 * Class ini berfungsi sebagai menampilkan data
 * seperti id, nama, dan alamat kemudian
 * ditampilkan ke dalam listview.
 */
public class AdapterMovieDetail extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataMovieDetail> items;

    AdapterMovieDetail context = AdapterMovieDetail.this;

    public AdapterMovieDetail(Activity activity, List<DataMovieDetail> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null){
            convertView = inflater.inflate(R.layout.activity_detail,null);
        }

        /*TextView md_title = (TextView)convertView.findViewById(R.id.txtTitle);
        ImageView md_poster = (ImageView)convertView.findViewById(R.id.imgPoster_path);
        TextView md_overview = (TextView)convertView.findViewById(R.id.txtOverview);
        TextView md_vote_average = (TextView)convertView.findViewById(R.id.txtVote_average);
        TextView md_release_date = (TextView)convertView.findViewById(R.id.txtRelease_date);

        DataMovieDetail data = items.get(position);
        md_title.setText(data.getMd_title());
        md_overview.setText(data.getMd_overview());
        md_vote_average.setText(data.getMd_vote_average());
        md_release_date.setText(data.getMd_release_date());

        String img_get = "http://image.tmdb.org/t/p/w185" + data.getMd_path();
        Log.e("img get : ", img_get);
        Picasso.with(context.activity).load(img_get)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(md_poster);*/

        return convertView;
    }
}
