package com.uin.firman.fedora.mupi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.data.DataMovieMain;

import java.util.List;

/**
 * Created by koko on 4/13/16.
 *
 * Buat class Adapter.java didalam package adapter dan tambahkan coding seperti dibawah ini.
 * Class ini berfungsi sebagai menampilkan data
 * seperti id, nama, dan alamat kemudian
 * ditampilkan ke dalam listview.
 */
public class AdapterMovieMain extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataMovieMain> items;

    AdapterMovieMain context = AdapterMovieMain.this;

    public AdapterMovieMain(Activity activity, List<DataMovieMain> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null){
            convertView = inflater.inflate(R.layout.movie_main,null);
        }

        TextView id_movie = (TextView)convertView.findViewById(R.id.movie_id);
        ImageView img_movie = (ImageView)convertView.findViewById(R.id.imageView_movie_main_id);

        DataMovieMain data = items.get(position);
        id_movie.setText(data.getMovie_id());
        //String img_get = "http://image.tmdb.org/t/p/w185/lIv1QinFqz4dlp5U4lQ6HaiskOZ.jpg";
        String img_get = "http://image.tmdb.org/t/p/w185" + data.getMovie_path();
        Log.e("img get : ", img_get);
        Picasso.with(context.activity).load(img_get)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(img_movie);
        return convertView;
    }
}
