package com.uin.firman.fedora.mupi.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.data.DataMovieTrailer;

import java.util.List;

/**
 * Created by koko on 4/13/16.
 *
 * Buat class Adapter.java didalam package adapter dan tambahkan coding seperti dibawah ini.
 * Class ini berfungsi sebagai menampilkan data
 * seperti id, nama, dan alamat kemudian
 * ditampilkan ke dalam listview.
 */
public class AdapterMovieTrailer extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataMovieTrailer> items;

    AdapterMovieTrailer context = AdapterMovieTrailer.this;

    public AdapterMovieTrailer(Activity activity, List<DataMovieTrailer> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null){
            convertView = inflater.inflate(R.layout.trailer,null);
        }

        TextView ntname = (TextView)convertView.findViewById(R.id.txtTrailerName);
        ImageView ntkey = (ImageView)convertView.findViewById(R.id.imageView_Trailer);

        DataMovieTrailer data = items.get(position);
        ntname.setText(data.getMtname());
        //String img_get = "http://image.tmdb.org/t/p/w185/5N20rQURev5CNDcMjHVUZhpoCNC.jpg";
        String img_get = "http://img.youtube.com/vi/"+data.getMtkey()+"/0.jpg";
        Log.e("img get : ", img_get);
        Picasso.with(context.activity).load(img_get)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(ntkey);

        return convertView;
    }
}
