package com.uin.firman.fedora.mupi.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.data.DataMovieMain;

import java.util.ArrayList;

/**
 * Created by koko on 5/13/16.
 */
public class MainAdapter extends BaseAdapter {

    Context c;
    ArrayList<DataMovieMain> dataMovieMains;
    LayoutInflater inflater;

    public MainAdapter(Context c, ArrayList<DataMovieMain> item) {
        this.c = c;
        this.dataMovieMains = item;
    }

    @Override
    public int getCount() {
        return dataMovieMains.size();
    }

    @Override
    public Object getItem(int position) {
        return dataMovieMains.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater==null){
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.movie_main,parent,false);
        }

        TextView id_movie = (TextView)convertView.findViewById(R.id.movie_id);
        ImageView img_movie = (ImageView)convertView.findViewById(R.id.imageView_movie_main_id);

        DataMovieMain data = dataMovieMains.get(position);
        id_movie.setText(data.getMovie_id());
        //String img_get = "http://image.tmdb.org/t/p/w185/lIv1QinFqz4dlp5U4lQ6HaiskOZ.jpg";
        String img_get = "http://image.tmdb.org/t/p/w185" + data.getMovie_path();
        Log.e("img get : ", img_get);
        Picasso.with(c).load(img_get)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(img_movie);
        return convertView;
    }
}
