package com.uin.firman.fedora.mupi;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.uin.firman.fedora.mupi.Adapter.AdapterMovieTrailer;
import com.uin.firman.fedora.mupi.app.AppController;
import com.uin.firman.fedora.mupi.data.DataMovieTrailer;
import com.uin.firman.fedora.mupi.db.DbMupi;
import com.uin.firman.fedora.mupi.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    String xTitle,xId;
    FloatingActionButton fab2, fab3;

    private static final String TAG = DetailActivity.class.getSimpleName();
    GridView list;
    SwipeRefreshLayout swipe;
    List<DataMovieTrailer> itemListsTrailer = new ArrayList<DataMovieTrailer>();
    AdapterMovieTrailer adapterMovieTrailer;

    String tag_json_obj = "json_obj_req";

    ImageView md_poster, posterAtas;
    TextView md_title, md_overview, md_vote_average,md_release_date, md_content;

    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        xTitle = extras.getString("xTitle");
        xId = extras.getString("xId");

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);

        localdb_cekIdmupi(xId);

        posterAtas = (ImageView)findViewById(R.id.imgposterlandscape_Id);

        md_title = (TextView)findViewById(R.id.txtTitle);
        md_poster = (ImageView)findViewById(R.id.imgPoster_path);
        md_overview = (TextView)findViewById(R.id.txtOverview);
        md_vote_average = (TextView)findViewById(R.id.txtVote_average);
        md_release_date = (TextView)findViewById(R.id.txtRelease_date);
        md_content = (TextView)findViewById(R.id.txtContent);

        swipe   = (SwipeRefreshLayout) findViewById(R.id.detail_swipe_refresh_layout);
        list    = (GridView) findViewById(R.id.grid_view_trailer);

        adapterMovieTrailer = new AdapterMovieTrailer(DetailActivity.this, itemListsTrailer);
        list.setAdapter(adapterMovieTrailer);
        swipe.setOnRefreshListener(this);

        /*itemListsTrailer.clear();
        adapterMovieTrailer.notifyDataSetChanged();
        load_movie_trailer();
        load_movie_detail();
        load_movie_reviews();*/

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           itemListsTrailer.clear();
                           adapterMovieTrailer.notifyDataSetChanged();
                           load_movie_trailer();
                           load_movie_detail();
                           load_movie_reviews();
                       }
                   }
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                String xKey = itemListsTrailer.get(position).getMtkey();
                Uri uri = Uri.parse("https://www.youtube.com/watch?v=" + xKey);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbMupi db = new DbMupi(DetailActivity.this);
                db.open();
                db.insertDataMupi(xId);
                db.close();
                Snackbar.make(view, "Mark as favorite movie.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                fab2.setVisibility(View.GONE);fab3.setVisibility(View.VISIBLE);

                f5();

            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbMupi db = new DbMupi(DetailActivity.this);
                db.open();
                db.DeleteIdMupi(xId);
                db.close();
                Snackbar.make(view, "Remove from favorite movie.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                fab3.setVisibility(View.GONE);
                fab2.setVisibility(View.VISIBLE);

                f5();

            }
        });

    }

    private void f5() {



        /*FavoriteFragment frg = null;
        frg = (FavoriteFragment) getSupportFragmentManager().findFragmentById(R.id.ff_id);
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();*/

        /*FavoriteFragment articleFrag = (FavoriteFragment)
        getSupportFragmentManager().findFragmentById(R.id.ff_id);*/
        /*if (articleFrag != null) {
            articleFrag.onRefresh();
        }*//* else {
            ArticleFragment newFragment = new ArticleFragment();
            Bundle args = new Bundle();
            args.putInt(ArticleFragment.ARG_POSITION, position);
            newFragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        }*/
    }

    private void load_movie_reviews() {

        itemListsTrailer.clear();
        adapterMovieTrailer.notifyDataSetChanged();
        swipe.setRefreshing(true);

        StringRequest strResq = new StringRequest(Request.Method.GET, "https://api.themoviedb.org/3/movie/"+xId+"/reviews?api_key="+ Server.key_api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Jawab : " + response.toString());

                try {
                    JSONObject jObj1 = new JSONObject(response);
                    JSONArray jsonArray1 = jObj1.getJSONArray("results");
                    for(int i=0; i<jsonArray1.length();i++)
                    {
                        JSONObject jObj2 = jsonArray1.getJSONObject(i);
                        String author = jObj2.getString("author");
                        String content = jObj2.getString("content");
                        Log.d("jwj : ", author + "/" + content);
                        md_content.setText("Author: " + author+"\n"+content);
                    }
                    adapterMovieTrailer.notifyDataSetChanged();
                    swipe.setRefreshing(false);

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                /*swipe.setRefreshing(false);*/
            }
        });
        AppController.getInstance().addToRequestQueue(strResq, tag_json_obj);
    }

    private void load_movie_detail() {

        itemListsTrailer.clear();
        adapterMovieTrailer.notifyDataSetChanged();
        swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.GET, "https://api.themoviedb.org/3/movie/"+xId+"?api_key="+ Server.key_api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Jawab : " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    String title = jObj.getString("title");
                    String poster_path = jObj.getString("poster_path");
                    String overview = jObj.getString("overview");
                    String vote_average = jObj.getString("vote_average");
                    String vote_count = jObj.getString("vote_count");
                    String release_date = jObj.getString("release_date");
                    //Log.d("Jawab md : ", release_date+"-"+vote_average+"/"+vote_count);

                    md_title.setText(title);
                    md_overview.setText(overview);
                    md_vote_average.setText(vote_average + "/" + vote_count);
                    md_release_date.setText(release_date);

                    String img_get = "http://image.tmdb.org/t/p/w185" + poster_path;
                    Picasso.with(DetailActivity.this).load(img_get)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.loading)
                            .into(md_poster);

                    /*String img_get2 = "http://image.tmdb.org/t/p/w185" + poster_path;
                    Picasso.with(DetailActivity.this).load(img_get2)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.loading)
                            .into(posterAtas);*/

                    adapterMovieTrailer.notifyDataSetChanged();
                    swipe.setRefreshing(false);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void load_movie_trailer() {

        itemListsTrailer.clear();
        adapterMovieTrailer.notifyDataSetChanged();
        swipe.setRefreshing(true);

        StringRequest strResq = new StringRequest(Request.Method.GET, "https://api.themoviedb.org/3/movie/"+xId+"/videos?api_key="+ Server.key_api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Jawab : " + response.toString());

                try {
                    JSONObject jObj1 = new JSONObject(response);
                    JSONArray jsonArray1 = jObj1.getJSONArray("results");
                    for(int i=0; i<jsonArray1.length();i++)
                    {
                        JSONObject jObj2 = jsonArray1.getJSONObject(i);
                        String name = jObj2.getString("name");
                        String key = jObj2.getString("key");
                        Log.d("jwj : ", key + "/" + name);

                        DataMovieTrailer item = new DataMovieTrailer();
                        item.setMtname(name);
                        item.setMtkey(key);
                        itemListsTrailer.add(item);
                    }

                    JSONObject jObj2_x = jsonArray1.getJSONObject(0);
                    String key_x = jObj2_x.getString("key");
                    String img_get = "http://img.youtube.com/vi/"+key_x+"/0.jpg";
                    Picasso.with(DetailActivity.this).load(img_get)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.loading)
                            .into(posterAtas);

                    adapterMovieTrailer.notifyDataSetChanged();
                    swipe.setRefreshing(false);

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                /*swipe.setRefreshing(false);*/
            }
        });
        AppController.getInstance().addToRequestQueue(strResq, tag_json_obj);
    }

    private void localdb_cekIdmupi(String xId) {
        DbMupi db = new DbMupi(DetailActivity.this);
        db.open();
        Cursor cur = db.cekIdMupi(xId);
        if(cur.getCount()!=0){
            cur.moveToFirst();
            fab3.setVisibility(View.VISIBLE);
        }else {
            fab2.setVisibility(View.VISIBLE);
        }
        db.close();
    }

    @Override
    public void onRefresh() {
        itemListsTrailer.clear();
        adapterMovieTrailer.notifyDataSetChanged();
        load_movie_trailer();
        load_movie_detail();
        load_movie_reviews();
    }
}
