package com.uin.firman.fedora.mupi.Fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uin.firman.fedora.mupi.Adapter.MainAdapter;
import com.uin.firman.fedora.mupi.DetailActivity;
import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.app.AppController;
import com.uin.firman.fedora.mupi.data.DataMovieMain;
import com.uin.firman.fedora.mupi.db.DbMupi;
import com.uin.firman.fedora.mupi.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by koko on 5/13/16.
 */
public class FavoriteFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    String tag_json_obj = "json_obj_req";
    private static final String TAG = FavoriteFragment.class.getSimpleName();
    public static final String TAGG = FavoriteFragment.class.getSimpleName();

    GridView list;
    SwipeRefreshLayout swipe;
    List<DataMovieMain> itemList = new ArrayList<DataMovieMain>();
    MainAdapter adapter;
    //Context c;

    //ImageView imgfn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.favorite_fragment,container,false);

        swipe   = (SwipeRefreshLayout)rootView.findViewById(R.id.fswipe_refresh_layout);
        list    = (GridView)rootView.findViewById(R.id.ffavorite_grid_view_id);

        //imgfn = (ImageView)rootView.findViewById(R.id.imgfn_id);

        adapter = new MainAdapter(this.getActivity(), (ArrayList<DataMovieMain>) itemList);
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(this);

        /*sengaja di komen karena cukup di onResume() saja supaya data gak muncul dobel*/
        /*swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           itemList.clear();
                           adapter.notifyDataSetChanged();
                           cekCountIdMupi();
                       }
                   }
        );*/

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                String xTitle = itemList.get(position).getTitle();
                String xId = itemList.get(position).getMovie_id();
                if(xTitle!=""&&xId!="")
                {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra("xTitle", xTitle);
                    intent.putExtra("xId", xId);
                    startActivity(intent);
                }

            }
        });

        //ListView lv = (ListView)rootView.findViewById(R.id.topratedListViewID);
        //CustomAdapter adapter = new CustomAdapter(this.getActivity(),getNameDev());
        //lv.setAdapter(adapter);

        return rootView;

    }

    public void cekCountIdMupi() {
        DbMupi db = new DbMupi(getActivity());
        db.open();
        Cursor cur = db.getListIdMupi();
        if(cur.getCount()!=0){
            Log.d("Cek Count", "Berisi: " + cur.getCount());
            //imgfn.setVisibility(View.GONE);

            DbMupi db2 = new DbMupi(getActivity());
            db.open();
            Cursor cur2 = db.getListIdMupi();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {
                        Log.d("P1", String.valueOf(cur2.getCount()));
                        do {
                            String dir = cur2.getString(cur2.getColumnIndex("idmupi"));
                            Log.d("P1: ", dir);
                            itemList.clear();
                            adapter.notifyDataSetChanged();
                            load_movie(dir);
                        } while (cur2.moveToNext());
                    } else {
                        swipe.setRefreshing(false);
                        //imgfn.setVisibility(View.VISIBLE);
                        //Toast.makeText(getActivity(), "Favorite Movie is Empty!!", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                swipe.setRefreshing(false);
                //imgfn.setVisibility(View.VISIBLE);
                //Toast.makeText(getActivity(), "Favorite Movie is Empty!", Toast.LENGTH_LONG).show();
            }
            db.close();

        }else {
            Log.d("Cek Count", "Kosong");
            swipe.setRefreshing(false);
            //imgfn.setVisibility(View.VISIBLE);
            //Toast.makeText(getActivity(), "Favorite Movie is Empty!", Toast.LENGTH_LONG).show();
        }
        db.close();
    }

    private void load_movie(String dir) {

        itemList.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.GET, "https://api.themoviedb.org/3/movie/"+dir+"?api_key="+Server.key_api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "P1 : " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    String id = jObj.getString("id");
                    String poster_path = jObj.getString("poster_path");
                    String title = jObj.getString("title");
                    Log.d("P2 : ", id+"-"+poster_path+"-"+title);

                    DataMovieMain item = new DataMovieMain();
                    item.setMovie_id(id);
                    item.setMovie_path(poster_path);
                    item.setTitle(title);
                    itemList.add(item);
                    adapter.notifyDataSetChanged();
                    swipe.setRefreshing(false);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "no internet connection!", Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public String toString() {
        String title = "Favorite";
        return title;
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        cekCountIdMupi();
    }

    @Override
    public void onResume() {
        super.onResume();
        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           itemList.clear();
                           adapter.notifyDataSetChanged();
                           cekCountIdMupi();
                       }
                   }
        );
    }

}
