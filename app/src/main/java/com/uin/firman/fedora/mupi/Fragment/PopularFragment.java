package com.uin.firman.fedora.mupi.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uin.firman.fedora.mupi.Adapter.MainAdapter;
import com.uin.firman.fedora.mupi.DetailActivity;
import com.uin.firman.fedora.mupi.R;
import com.uin.firman.fedora.mupi.app.AppController;
import com.uin.firman.fedora.mupi.data.DataMovieMain;
import com.uin.firman.fedora.mupi.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by koko on 5/13/16.
 */
public class PopularFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    String tag_json_obj = "json_obj_req";
    private static final String TAG = PopularFragment.class.getSimpleName();

    GridView list;
    SwipeRefreshLayout swipe;
    List<DataMovieMain> itemList = new ArrayList<DataMovieMain>();
    MainAdapter adapter;
    Context c;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popular_fragment,container,false);

        swipe   = (SwipeRefreshLayout)rootView.findViewById(R.id.pswipe_refresh_layout);
        list    = (GridView)rootView.findViewById(R.id.ppopular_grid_view_id);

        adapter = new MainAdapter(this.getActivity(), (ArrayList<DataMovieMain>) itemList);
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           itemList.clear();
                           adapter.notifyDataSetChanged();
                           load_movie();
                       }
                   }
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                String xTitle = itemList.get(position).getTitle();
                String xId = itemList.get(position).getMovie_id();
                if(xTitle!=""&&xId!="")
                {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra("xTitle", xTitle);
                    intent.putExtra("xId", xId);
                    startActivity(intent);
                }

            }
        });

        //ListView lv = (ListView)rootView.findViewById(R.id.topratedListViewID);
        //CustomAdapter adapter = new CustomAdapter(this.getActivity(),getNameDev());
        //lv.setAdapter(adapter);

        return rootView;

    }

    private void load_movie() {

        itemList.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.GET, Server.url_popular, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Jawab : " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    //String page = jObj.getString("page");
                    //Log.d("Jawab : ", page);
                    JSONArray resultsArray = jObj.getJSONArray("results");
                    for(int i=0; i<resultsArray.length();i++)
                    {
                        JSONObject objResults = resultsArray.getJSONObject(i);
                        //String id = objResults.getString("id");
                        //String poster_path = objResults.getString("poster_path");
                        //Log.d("Jawab : ID=", id + " Foto= " + poster_path);
                        DataMovieMain item = new DataMovieMain();
                        item.setMovie_id(objResults.getString("id"));
                        item.setMovie_path(objResults.getString("poster_path"));
                        item.setTitle(objResults.getString("title"));
                        itemList.add(item);

                    }
                    adapter.notifyDataSetChanged();
                    swipe.setRefreshing(false);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "no internet connection!", Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public String toString() {
        String title = "Popular";
        return title;
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        load_movie();
    }

    /*@Override
    public void onResume() {
        super.onResume();
        itemList.clear();
        adapter.notifyDataSetChanged();
        load_movie();
    }*/
}
