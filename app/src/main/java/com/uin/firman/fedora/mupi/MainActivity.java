package com.uin.firman.fedora.mupi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.uin.firman.fedora.mupi.Adapter.MyPagerAdapter;
import com.uin.firman.fedora.mupi.Fragment.FavoriteFragment;
import com.uin.firman.fedora.mupi.Fragment.PopularFragment;
import com.uin.firman.fedora.mupi.Fragment.TopratedFragment;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    ViewPager vp;
    TabLayout tp;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.logo2);
        toolbar.setTitle("Mupi");
        toolbar.setSubtitle("Movie info from Themoviedb");
        setSupportActionBar(toolbar);

        /*view pager*/
        vp = (ViewPager)findViewById(R.id.mViewpager_ID);
        this.addPages();

        /*tab layout*/
        tp = (TabLayout)findViewById(R.id.mTab_ID);
        tp.setTabGravity(TabLayout.GRAVITY_FILL);
        tp.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        tp.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorWhite));
        tp.setupWithViewPager(vp);
        tp.setOnTabSelectedListener(this);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    private void addPages(){
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(this.getSupportFragmentManager());
        pagerAdapter.addFragment(new PopularFragment());
        pagerAdapter.addFragment(new TopratedFragment());
        pagerAdapter.addFragment(new FavoriteFragment());
        /*set adapter to vp*/
        vp.setAdapter(pagerAdapter);
        //vp.setCurrentItem(2);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        vp.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //return true;
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Mupi App");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Info Movie popular and top rated. [Firman UIN]");
            startActivity(Intent.createChooser(sharingIntent, "Share Mupi App via"));
        }

        return super.onOptionsItemSelected(item);
    }
}
