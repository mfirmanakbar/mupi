package com.uin.firman.fedora.mupi.data;

/**
 * Created by koko on 5/12/16.
 */
public class DataMovieDetail {
    public String md_title, md_poster_path, md_overview, md_vote_average, md_release_date;

    public DataMovieDetail(){

    }

    public DataMovieDetail(String md_title, String md_poster_path, String md_overview, String md_vote_average, String md_release_date) {
        this.md_title = md_title;
        this.md_poster_path = md_poster_path;
        this.md_overview = md_overview;
        this.md_vote_average = md_vote_average;
        this.md_release_date = md_release_date;
    }

    public String getMd_title() {
        return md_title;
    }

    public void setMd_title(String md_title) {
        this.md_title = md_title;
    }

    public String getMd_poster_path() {
        return md_poster_path;
    }

    public void setMd_poster_path(String md_poster_path) {
        this.md_poster_path = md_poster_path;
    }

    public String getMd_overview() {
        return md_overview;
    }

    public void setMd_overview(String md_overview) {
        this.md_overview = md_overview;
    }

    public String getMd_vote_average() {
        return md_vote_average;
    }

    public void setMd_vote_average(String md_vote_average) {
        this.md_vote_average = md_vote_average;
    }

    public String getMd_release_date() {
        return md_release_date;
    }

    public void setMd_release_date(String md_release_date) {
        this.md_release_date = md_release_date;
    }
}
