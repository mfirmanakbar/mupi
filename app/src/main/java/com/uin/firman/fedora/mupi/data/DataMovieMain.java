package com.uin.firman.fedora.mupi.data;

/**
 * Created by koko on 4/24/16.
 */
public class DataMovieMain {
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String title;
    public String getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(String movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_path() {
        return movie_path;
    }

    public void setMovie_path(String movie_path) {
        this.movie_path = movie_path;
    }

    private String movie_id, movie_path;

    public DataMovieMain() {
    }

    public DataMovieMain(String movie_id, String movie_path, String title) {
        this.movie_id = movie_id;
        this.movie_path = movie_path;
        this.title = title;
    }
}
