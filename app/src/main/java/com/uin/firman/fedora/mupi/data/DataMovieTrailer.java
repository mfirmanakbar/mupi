package com.uin.firman.fedora.mupi.data;

/**
 * Created by koko on 5/12/16.
 */
public class DataMovieTrailer {

    public DataMovieTrailer() {
    }

    public DataMovieTrailer(String mtkey, String mtname) {
        this.mtkey=mtkey;
        this.mtname= mtname;
    }

    public String mtname;

    public String getMtkey() {
        return mtkey;
    }

    public void setMtkey(String mtkey) {
        this.mtkey = mtkey;
    }

    public String getMtname() {
        return mtname;
    }

    public void setMtname(String mtname) {
        this.mtname = mtname;
    }

    public String mtkey;
}
