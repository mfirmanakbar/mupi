package com.uin.firman.fedora.mupi.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbMupi {
	private SQLiteDatabase db;
	private final Context con;
	private final DbOpenHelper dbHelper;

	public DbMupi(Context con) {
		this.con = con;
		dbHelper = new DbOpenHelper(this.con, "", null, 0);
	}
	
	public void open() {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		db.close();
	}
	
	public long insertDataMupi(String idMupi) {
		ContentValues newValues = new ContentValues();
		newValues.put("idmupi", idMupi);
		return db.insert("tb_favorite", null, newValues);
	}

	public Cursor cekIdMupi(String idmupi){
		return db.rawQuery("SELECT * FROM tb_favorite where idmupi=?", new String[]{idmupi});
	}

	public Cursor countIdMupi(){
		return db.rawQuery("SELECT count(idmupi) FROM tb_favorite", null);
	}

	public long DeleteIdMupi(String idmupi) {
		return db.delete("tb_favorite", "idmupi=" + idmupi, null);
	}

	public Cursor getListIdMupi(){
		return db.rawQuery("SELECT * FROM tb_favorite ORDER BY _id DESC", null);
	}

/*	public void resetDb(){
		dbHelper.onUpgrade(db, 0, 0);
	}*/
}
