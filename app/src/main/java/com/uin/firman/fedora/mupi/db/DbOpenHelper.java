package com.uin.firman.fedora.mupi.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "mupi.db";
	private static final String TABLE_REGISTER_CREATE =
		"CREATE TABLE tb_favorite (_id INTEGER PRIMARY KEY AUTOINCREMENT, idmupi TEXT)";

	public DbOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_REGISTER_CREATE);

		//Contoh Insert
		/*ContentValues cv = new ContentValues();
		cv.put("idmupi", "TES");
		db.insert("tb_favorite", null, cv);*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		db.execSQL("DROP TABLE IF EXISTS tb_favorite");
		onCreate(db);
	}

}
